package com.thecodemaster.thecodemaster.models;

import com.thecodemaster.thecodemaster.repository.BlogPostStatusesRepository;
import com.thecodemaster.thecodemaster.repository.BlogRepository;
import com.thecodemaster.thecodemaster.repository.CommentsRepository;
import com.thecodemaster.thecodemaster.repository.PostGroupRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(properties = {
		"spring.jpa.hibernate.ddl-auto=create-drop",
		"spring.liquibase.enabled=false",
		"spring.flyway.enabled=false"
})
@AutoConfigureMockMvc
public class CommentsTest {
	@Autowired
	private CommentsRepository commentsRepository;

	/* blog */
	@Autowired
	private BlogRepository blogRepository;
	@Autowired
	private BlogPostStatusesRepository blogPostStatusesRepository;
	@Autowired
	private PostGroupRepository postGroupRepository;

	private Blog prepareBlogPost() {
		Integer groupId = 0;

		/* save fakes */
		BlogPostStatuses statusSaved = blogPostStatusesRepository.findById(1).orElse(new BlogPostStatuses());
		PostGroup postGroup = postGroupRepository.findById(groupId).orElse(new PostGroup());

		Blog blog = new Blog(1L, 1, "12ee234rrf31", postGroup, "Test Blog Post", "<p>Test contents</p>", statusSaved, 123, 123, LocalDate.now());
		blogRepository.save(blog);

		return blog;
	}

	@Test
	public void testObjectStructure() {
		String expectedComment = "Awesome, my first comment";
		Blog blog = this.prepareBlogPost();

		Comments comment = new Comments(1L, "Test User", "Awesome, my first comment", 12, blog);
		commentsRepository.save(comment);

		String actualComment = commentsRepository.findById(1L).orElse(new Comments()).getComment();
		assertThat(expectedComment).isEqualTo(actualComment);
	}
}
