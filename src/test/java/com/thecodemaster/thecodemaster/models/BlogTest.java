package com.thecodemaster.thecodemaster.models;

import com.thecodemaster.thecodemaster.repository.BlogPostStatusesRepository;
import com.thecodemaster.thecodemaster.repository.BlogRepository;
import com.thecodemaster.thecodemaster.repository.PostGroupRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.*;

import java.time.LocalDate;


@SpringBootTest(properties = {
		"spring.jpa.hibernate.ddl-auto=create-drop",
		"spring.liquibase.enabled=false",
		"spring.flyway.enabled=false"
})
@AutoConfigureMockMvc
public class BlogTest {
	/* Main Dependencies */
	@Autowired
	private BlogRepository blogRepository;
	@Autowired
	private BlogPostStatusesRepository blogPostStatusesRepository;
	@Autowired
	private PostGroupRepository postGroupRepository;

	@Test
	public void testObjectStructure(){
		/* fakes */
		Long id = 1L;
		Integer groupId = 0;
		Integer statusId = 1;
		String hashId = "12ee234rrf31";
		String title = "Test Blog Post";

		/* save fakes */
		BlogPostStatuses statusSaved = blogPostStatusesRepository.findById(statusId).orElse(new BlogPostStatuses());
		PostGroup postGroup = postGroupRepository.findById(groupId).orElse(new PostGroup());

		Blog blog = new Blog(id, 1, hashId, postGroup, title, "<p>Test contents</p>", statusSaved, 123, 123, LocalDate.now());
		blogRepository.save(blog);

		Blog actualObjById = blogRepository.findById(id).orElse(new Blog());
		Blog actualObjByHashId = blogRepository.findByHashId(hashId);

		String actualTitleById = blogRepository.findById(id).orElse(new Blog()).getTitle();
		String actualTitleByHashId = blogRepository.findByHashId(hashId).getTitle();

		/* assertion */
		assertThat(title).isEqualTo(actualTitleById);
		assertThat(title).isEqualTo(actualTitleByHashId);
		Assertions.assertTrue(actualObjById instanceof Blog);
		Assertions.assertNotNull(actualObjByHashId);
	}
}
