package com.thecodemaster.thecodemaster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThecodemasterApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThecodemasterApplication.class, args);
	}

}
