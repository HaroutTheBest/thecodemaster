package com.thecodemaster.thecodemaster.services;


import com.thecodemaster.thecodemaster.repository.CommentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentsService {
	private final CommentsRepository commentsRepository;

	@Autowired
	public CommentsService(CommentsRepository commentsRepository) {
		this.commentsRepository = commentsRepository;
	}
}
