package com.thecodemaster.thecodemaster.services;

import com.thecodemaster.thecodemaster.repository.PostGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostGroupService {
	private final PostGroupRepository postGroupRepository;

	@Autowired
	public PostGroupService (PostGroupRepository postGroupRepository) {
		this.postGroupRepository = postGroupRepository;
	}
}
