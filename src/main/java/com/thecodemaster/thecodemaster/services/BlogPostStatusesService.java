package com.thecodemaster.thecodemaster.services;

import com.thecodemaster.thecodemaster.repository.BlogPostStatusesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BlogPostStatusesService {
	private final BlogPostStatusesRepository blogPostStatusesRepository;

	@Autowired
	public BlogPostStatusesService(BlogPostStatusesRepository blogPostStatusesRepository) {
		this.blogPostStatusesRepository = blogPostStatusesRepository;
	}
}
