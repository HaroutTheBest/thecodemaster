package com.thecodemaster.thecodemaster.config.models;

import com.thecodemaster.thecodemaster.models.BlogPostStatuses;
import com.thecodemaster.thecodemaster.repository.BlogPostStatusesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
class BlogPostStatusesConfig {
	private final BlogPostStatusesRepository blogPostStatusesRepository;

	@Autowired
	public BlogPostStatusesConfig(BlogPostStatusesRepository blogPostStatusesRepository) {
		this.blogPostStatusesRepository = blogPostStatusesRepository;
	}

	@Bean
	public void saveBps() {
		BlogPostStatuses draft = new BlogPostStatuses(
			0, "draft"
		);
		BlogPostStatuses saved = new BlogPostStatuses(
			1, "saved"
		);
		BlogPostStatuses archived = new BlogPostStatuses(
			2, "archived"
		);
		blogPostStatusesRepository.saveAll(List.of(draft, saved, archived));
	}
}
