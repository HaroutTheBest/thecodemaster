package com.thecodemaster.thecodemaster.config.models;

import com.thecodemaster.thecodemaster.models.PostGroup;
import com.thecodemaster.thecodemaster.repository.PostGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
class PostGroupConfig {
	private final PostGroupRepository postGroupRepository;

	@Autowired
	public PostGroupConfig (PostGroupRepository postGroupRepository) {
		this.postGroupRepository = postGroupRepository;
	}

	@Bean
	public void savePg() {
		PostGroup noGroup = new PostGroup(0, "noGroup");
		postGroupRepository.saveAll(List.of(noGroup));
	}
}
