package com.thecodemaster.thecodemaster.repository;


import com.thecodemaster.thecodemaster.models.PostGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostGroupRepository extends JpaRepository<PostGroup, Integer> {
}
