package com.thecodemaster.thecodemaster.repository;

import com.thecodemaster.thecodemaster.models.Comments;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentsRepository extends JpaRepository<Comments, Long> {
}
