package com.thecodemaster.thecodemaster.repository;

import com.thecodemaster.thecodemaster.models.Blog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BlogRepository extends JpaRepository<Blog, Long> {
	@Query("select b from Blog b where b.hashId = ?1")
	Blog findByHashId(String hashId);
}
