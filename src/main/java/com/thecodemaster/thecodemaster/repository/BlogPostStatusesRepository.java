package com.thecodemaster.thecodemaster.repository;

import com.thecodemaster.thecodemaster.models.BlogPostStatuses;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlogPostStatusesRepository extends CrudRepository<BlogPostStatuses, Integer> {
}
