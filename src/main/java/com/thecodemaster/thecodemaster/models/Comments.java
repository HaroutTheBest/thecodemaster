package com.thecodemaster.thecodemaster.models;

import javax.persistence.*;

@Entity
@Table
public class Comments {
	@Id
	@SequenceGenerator(
			name = "comment_sequence",
			sequenceName = "comment_sequence",
			allocationSize = 1
	)
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "comment_sequence"
	)
	private Long id;
	private String c_name;
	private String comment;
	private Integer claps;

	@ManyToOne
	private Blog post;

	public Comments(Long id, String c_name, String comment, Integer claps, Blog post) {
		this.id = id;
		this.c_name = c_name;
		this.comment = comment;
		this.claps = claps;
		this.post = post;
	}

	public Comments() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getC_name() {
		return c_name;
	}

	public void setC_name(String c_name) {
		this.c_name = c_name;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getClaps() {
		return claps;
	}

	public void setClaps(Integer claps) {
		this.claps = claps;
	}

	public Blog getPost() {
		return post;
	}

	public void setPost(Blog post) {
		this.post = post;
	}
}
