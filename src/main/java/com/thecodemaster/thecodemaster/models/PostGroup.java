package com.thecodemaster.thecodemaster.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class PostGroup {
	@Id
	private Integer id;

	@Column
	private String name;


	public PostGroup(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public PostGroup() {

	}
}
