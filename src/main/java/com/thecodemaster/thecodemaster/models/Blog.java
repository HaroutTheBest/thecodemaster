package com.thecodemaster.thecodemaster.models;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(
		indexes = {
				@Index(name = "blog_hash_id", columnList = "hash_id"),
				@Index(name = "blog_title", columnList = "title")
		}
)
public class Blog {
	@Id
	@SequenceGenerator(
			name = "blog_sequence",
			sequenceName = "blog_sequence",
			allocationSize = 1
	)
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "blog_sequence"
	)
	private Long id;


	@Column(
			name = "user_id",
			unique = true,
			nullable = false
	)
	private Integer userId;


	@Column(
			name = "hash_id",
			unique = true,
			nullable = false,
			columnDefinition = "CHAR(12)"
	)
	private String hashId;


	@ManyToOne
	private PostGroup group;


	@Column(
			nullable = false
	)
	private String title;


	@Column(
			columnDefinition = "VARCHAR(2048)"
	)
	private String contents;


	@ManyToOne /* foreign key to -> [blog_post_status.id] */
	private BlogPostStatuses status;


	@Column(
			nullable = false
	)
	private Integer views = 0;


	@Column(
			nullable = false
	)
	private Integer claps = 0;


	@Column(
			name = "creation_date"
	)
	private LocalDate creationDate;


	public Blog(Long id, Integer userId, String hashId, PostGroup group, String title, String contents, BlogPostStatuses status, Integer views, Integer claps, LocalDate creationDate) {
		this.id = id;
		this.userId = userId;
		this.hashId = hashId;
		this.group = group;
		this.title = title;
		this.contents = contents;
		this.status = status;
		this.views = views;
		this.claps = claps;
		this.creationDate = creationDate;
	}

	public Blog() {

	}

	/* Getter and Setters */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setHashId(String hashId) {
		this.hashId = hashId;
	}

	public String getHashId() {
		return hashId;
	}

	public PostGroup getGroup() {
		return group;
	}

	public void setGroup(PostGroup group) {
		this.group = group;
	}

	public BlogPostStatuses getStatus() {
		return status;
	}

	public void setStatus(BlogPostStatuses status) {
		this.status = status;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public Integer getViews() {
		return views;
	}

	public void setViews(Integer views) {
		this.views = views;
	}

	public Integer getClaps() {
		return claps;
	}

	public void setClaps(Integer claps) {
		this.claps = claps;
	}

	public LocalDate getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public String toString() {
		return "Blog{" +
				"id=" + id +
				", userId=" + userId +
				", hashId='" + hashId + '\'' +
				", groupId=" + group +
				", title='" + title + '\'' +
				", contents='" + contents + '\'' +
				", status=" + status +
				", views=" + views +
				", claps=" + claps +
				", creationDate=" + creationDate +
				'}';
	}
}
