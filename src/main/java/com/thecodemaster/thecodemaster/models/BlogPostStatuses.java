package com.thecodemaster.thecodemaster.models;

import javax.persistence.*;

@Entity
@Table(
		name = "blog_post_status"
)
public class BlogPostStatuses {
	@Id
	private Integer id;


	@Column(
			length = 10
	)
	private String name;

	public BlogPostStatuses(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public BlogPostStatuses() {

	}
}
