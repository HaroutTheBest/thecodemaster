FROM openjdk:11
VOLUME /tmp
ADD /target/*.jar thecodemaster-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/thecodemaster-0.0.1-SNAPSHOT.jar"]
